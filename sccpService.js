const express = require('express');
const app = express();
const util = require('util');
const xml2js = require('xml2js');
const { exec } = require('child_process');


app.post('/', express.json(), (req, res) => {
    // let command = 'cd '+process.env.KROUTE + '&';
    // command += 'kompile -h';
    let program = req.body['sccpProgram'];
    let depth = req.body['depth'] || "";
    
    let command = `./script.sh "${program}" "${depth}"`

    console.log("Command", command);

    // Exec executes a command and once it's done executes the given callback
    const ls = exec(command, (err, stdout, stderr) => {
        if(err){
            console.error(err);
            res.send(err);
            return;
        }

        console.log('------BEGINNING OF OUTPUT------');
        // stdout = "<root>Hey!</root>";
        console.log(stdout);
        // var cleanedString = stdout.replace("\ufeff", "");
        let parsedString;
        
        function trim(str, name){
            if(name === 'spaces'){
                return str
                // Remove end and end spaces
                .trim()
                // Remove newlines between string
                .replace(/\n\s*/g, "")
                // Add a space between each space
                .replace(/\)root/g, ")\nroot")
                // Add a space between each fact
                .replace(/\)SetItem/g, ") SetItem")
                // Separate each space
                .split(/(?=root)/)
                // Remove the newline at the end of each space
                .map(s => s.trim())
                // Transform a space in an array on n elements where each element
                // is a subspace id and the last element is a group of knowledge
                // .map(s => s.split(/\s\.\s|\|\-\>/).map(subs => subs.trim()))
                // .reduce((partial, current, index) => {
                //     console.log('Calling to reduce', current);
                //     console.log("partial", partial);
                //     if (!partial) {
                //         console.log('creating partial');
                //         partial = {};
                //     }
                //     let level = partial;
                //     console.log("level", level);
                //     current.forEach(spaceId => {
                //         if(!level[spaceId]){
                //             let knowledge = spaceId.match(/SetItem \( \w \)/g) || spaceId.match(/.Set/);
                //             console.log('knowledge', knowledge);
                //             if(!knowledge) level[spaceId] = {};
                //             else {
                //                 if(knowledge[0] === '.Set') level['knowledge'] = [];
                //                 else level['knowledge'] = knowledge.map(fact => fact.match(/SetItem \( (\w) \)/)[1]);
                                
                //             };
                //         }
                //         level = level[spaceId];
                //     })
                //     return partial;
                // }, null);
            } else { 
                return str.trim();
            }
        }
        
        
        xml2js.parseString(stdout, {
            explicitArray: true,
            valueProcessors: [trim]
        }, (err, result) => {
            if (err) {
               console.log("Error ocurred", err); 
            }
            parsedString = result;
            console.log("Here is the parsed xml!", util.inspect(result,false, null));
            // Since we know there is only one array of spaces we only take the first
            parsedString.T.spaces = parsedString.T.spaces[0];
            console.log('------END OF OUTPUT------');
            res.send(parsedString);
            
        });
        // stdout = stdout.replace(/</g, '&lt');
        // stdout = stdout.replace(/>/g, '&gt');
        // stdout = stdout.replace(/\n/g, '<br>');
        // parsedString = xml2js.parseString(stdout);
        // res.send(parsedString);
    });
});


module.exports = app;