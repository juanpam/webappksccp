/**
 * Main file for the web app of the SCCP implementation
 */

const express = require('express');
const sccpService = require('./sccpService');
const cors = require('cors');
const app = express();

// Really important so it runs on heroku
const port = process.env.PORT || 5000;

// console.log('This is the k path', process.env.KROUTE)

// console.log('ENV', process.env);
app.use(cors())

app.get('/', (req, res) => res.send('Hello world!'));

app.use('/sccpService', sccpService);

app.listen(port, () => console.log(`WebAppKSccp is listening on port ${port}!`));